Source: mednafen
Maintainer: Debian Games Team <pkg-games-devel@lists.alioth.debian.org>
Uploaders: Stephen Kitt <skitt@debian.org>
Section: games
Priority: optional
Build-Depends: debhelper-compat (= 13),
               libasound2-dev [linux-any],
               libflac-dev,
               libgl-dev,
               libjack-dev,
               liblzo2-dev,
               libmpcdec-dev,
               libsamplerate0-dev,
               libsdl2-dev,
               libtrio-dev,
               libvorbisidec-dev,
               libzstd-dev,
               x11proto-dev,
               zlib1g-dev | libz-dev
Build-Conflicts: autoconf2.13
Standards-Version: 4.7.0
Vcs-Browser: https://salsa.debian.org/games-team/mednafen
Vcs-Git: https://salsa.debian.org/games-team/mednafen.git
Homepage: https://mednafen.github.io
Rules-Requires-Root: no

Package: mednafen
Architecture: any
Depends: ${misc:Depends},
         ${shlibs:Depends}
Recommends: mednaffe
Provides: nes-emulator
Description: multi-platform emulator, including NES, GB/A, Lynx, PC Engine
 Mednafen is a command-line driven emulator for many different systems. It
 has full support for OpenGL and SDL graphics, network play, remappable input
 configuration, joystick and keyboard support, save states, game rewinding,
 GSF playback, and screenshots.
 .
 The systems supported by Mednafen are:
    * Apple II/II+
    * Atari Lynx
    * GameBoy
    * GameBoy Color
    * GameBoy Advance
    * NES
    * SNES
    * Virtual Boy
    * PC Engine (TurboGrafx 16)
    * PC-FX
    * SuperGrafx
    * NeoGeo Pocket, NeoGeo Pocket Color
    * WonderSwan
    * Sega Genesis / Mega Drive
    * Sega Saturn
    * Sony PlayStation
 .
 Hardware emulated by Mednafen includes:
    * NES gamepad, Zapper, PowerPad
    * Four-Score, Famicom multiplayer adapter
    * Arkanoid, HyperShot, Space Shadow, Mahjong controllers
    * Oeka Kids tablet, Quiz King buzzers, Family Trainer, Barcode World
    * Game Genie
