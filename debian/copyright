Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: mednafen
Source: http://mednafen.sf.net
Copyright: 2005-2021 Mednafen Team
License: GPL-2+
Files-Excluded: tests/psx/*.exe

Files: *
Copyright: 2005-2024 Mednafen Team
License: GPL-2+

Files: debian/*
Copyright: 2005-2008 Ryan Schultz
           2010-2022, 2024-2025 Stephen Kitt
License: GPL-2+

Files: src/mpcdec/*
Comment: libmpcdec <http://www.musepack.net/>
Copyright: 2005 The Musepack Development Team
License: BSD-3-clause-Musepack
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are
 met:
 .
 * Redistributions of source code must retain the above copyright
 notice, this list of conditions and the following disclaimer.
 .
 * Redistributions in binary form must reproduce the above
 copyright notice, this list of conditions and the following
 disclaimer in the documentation and/or other materials provided
 with the distribution.
 .
 * Neither the name of the The Musepack Development Team nor the
 names of its contributors may be used to endorse or promote
 products derived from this software without specific prior
 written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

Files:
 src/mpcdec/datatypes.h
 src/mpcdec/mpcmath.h
Copyright: 2005 The Musepack Development Team
License: LGPL-2.1+

Files: src/mpcdec/minimax.h
Copyright: 1999-2004 Buschmann/Klemm/Piecha/Wolf
License: LGPL-2.1+

Files:
 src/nes/*
 src/pce_fast/huc6280_ops.inc
Comment: FCE Ultra <http://fceultra.sourceforge.net/>
Copyright: 1998 BERO
           2002 Xodnizel
           2002 Paul Kuliniewicz
           2003 CaH4e3
License: GPL-2+

Files: src/tremor/*
Comment: Tremor <http://xiph.org/>
Copyright: 1994-2014 Xiph.org Foundation
License: BSD-3-clause-Xiph

Files:
 src/hw_sound/gb_apu/*
 src/hw_sound/sms_apu/*
 src/hw_sound/ym2612/*
 src/nes/ntsc/*
 src/ngp/T6W28_Apu.cpp
 src/sound/Blip_Buffer.cpp
 src/sound/Fir_Resampler.cpp
 src/sound/Stereo_Buffer.cpp
Comment: Gb_Snd_Emu, Blip_Buffer, Sms_Snd_Emu, nes_ntsc <http://www.slack.net/~ant/libs/>
Copyright: 2003-2006 Shay Green
License: GPL-2+

Files: src/lynx/*
Comment: Handy <http://homepage.ntlworld.com/dystopia/>
Copyright: 2004 K. Wilkins
License: Zlib

Files: src/pce/pcecd.cpp src/pce_fast/huc.cpp src/pce_fast/pcecd.cpp
Comment: PC2e
Copyright: 2004 Ki
License: GPL-2+

Files: src/drivers/scale*
Comment: Scale2x <http://scale2x.sf.net/>
Copyright: 2001-2004 Andrea Mazzoleni
License: GPL-2+

Files: src/drivers/hq?x.cpp
Comment: hq2x, hq3x, hq4x <http://www.hiend3d.com/hq2x.html>
Copyright: 2003 MaxSt <maxst@hiend3d.com>
License: LGPL-2.1+

Files: src/drivers/2xSaI.*
Comment: 2xSaI
Copyright: 1999-2002 Derek Liauw Kie Fa
License: GPL-2+

Files:
 src/gb/gb*
 src/gb/gfx.cpp
 src/gb/memory.*
 src/gb/z80.cpp
 src/gba/*
Comment: VisualBoyAdvance <http://vba.sf.net/>
Copyright: 1999-2003 Forgotten
           2004-2006 Forgotten and the VBA development team
License: GPL-2+

Files: src/trio/*
Comment: trio <http://daniel.haxx.se/projects/trio/>
Copyright: 1998-2009 Bjorn Reese and Daniel Stenberg
License: trio
 Permission to use, copy, modify, and distribute this software for any
 purpose with or without fee is hereby granted, provided that the above
 copyright notice and this permission notice appear in all copies.
 .
 THIS SOFTWARE IS PROVIDED ``AS IS'' AND WITHOUT ANY EXPRESS OR IMPLIED
 WARRANTIES, INCLUDING, WITHOUT LIMITATION, THE IMPLIED WARRANTIES OF
 MERCHANTIBILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE AUTHORS AND
 CONTRIBUTORS ACCEPT NO RESPONSIBILITY IN ANY CONCEIVABLE MANNER.

Files: src/hw_cpu/v810/*
Comment: V810 Emulator
Copyright: 2006 David Tucker
License: GPL-2+

Files: src/hw_cpu/z80-fuse/*
Comment: Fuse Z80 emulation code <http://fuse-emulator.sourceforge.net/>
Copyright: 1999-2005 Philip Kendall, Witold Filipczyk
           1999-2011 Philip Kendall
License: GPL-2+

Files:
 src/minilzo/lzo*
 src/minilzo/minilzo.*
 src/minilzo/_minilzo.*
Comment: MiniLZO <http://www.oberhumer.com/opensource/lzo/>
Copyright: 1996-2015 Markus Franz Xaver Oberhumer
License: GPL-2+

Files: src/ngp/*
Comment: NeoPop Neo Geo Pocket (Color) Code <http://neopop.emuxhaven.net/>
Copyright: 2001-2002 neopop_uk
License: GPL-2+

Files: src/wswan/*
Comment: Cygne <http://cygne.emuunlim.com/>
Copyright: 2002 Dox <dox@space.pl>
License: GPL-2+

Files:
 src/wswan/v30mz*
Comment: NEC V30MZ Emulator
Copyright: Bryan McPhail
License: McPhail
 This NEC V30MZ emulator may be used for purposes both commercial and
 noncommercial if you give the author, Bryan McPhail, a small credit
 somewhere(such as in the documentation for an executable package).

Files: src/wswan/dis/*
Comment: NEC V30MZ disassembler (modified BOCHS x86 disassembler)
Copyright: 2005-2012 Stanislav Shwartsman
License: LGPL-2+

Files: src/quicklz/*
Comment: QuickLZ <http://www.quicklz.com/>
Copyright: 2006-2008 Lasse Mikkel Reinhold
License: GPL-1 or GPL-2
 QuickLZ can be used for free under the GPL-1 or GPL-2 license (where anything
 released into public must be open source) or under a commercial license if such
 has been acquired (see http://www.quicklz.com/order.html). The commercial license
 does not cover derived or ported versions created by third parties under GPL.
 .
 On Debian systems, the complete text of the GNU General
 Public License version 2 can be found in /usr/share/common-licenses/GPL-2

Files:
 src/cdrom/CDUtility.cpp
 src/cdrom/lec.*
Comment: Q-Subchannel CRC16 Code <http://cdrdao.sourceforge.net/>
Copyright: 1998-2002 Andreas Mueller <mueller@daneb.ping.de>
License: GPL-2+

Files: src/sms/*
Comment: SMS Plus <http://cgfm2.emuviews.com/>
Copyright: 1998-2004 Charles MacDonald
License: GPL-2+

Files: src/md/*
Comment: Genesis Plus <http://cgfm2.emuviews.com/>
Copyright: 1998-2003 Charles MacDonald
License: GPL-2+

Files: src/hw_sound/pce_psg/pce_psg.*
Copyright: 2001 Charles MacDonald
License: GPL-2+

Files:
 src/md/hvc.h
 src/md/cart/map_eeprom.cpp
Comment: Genesis Plus GX
Copyright: 2007-2009 EkeEke
License: GPL-2+

Files: src/md/cart/ssp16.*
Copyright: 2008 Gražvydas "notaz" Ignotas
License: BSD-3-clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:
     * Redistributions of source code must retain the above copyright
       notice, this list of conditions and the following disclaimer.
     * Redistributions in binary form must reproduce the above
       copyright notice, this list of conditions and the following
       disclaimer in the documentation and/or other materials provided
       with the distribution.
     * Neither the name of the organization nor the names of its
       contributors may be used to endorse or promote products derived
       from this software without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL COPYRIGHT
 HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
 WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 POSSIBILITY OF SUCH DAMAGE.

Files:
 src/hw_sound/ym2413/*
 src/nes/boards/emu2413.*
Comment: EMU2413
Copyright: 2004 Mitsutaka Okazaki
License: Zlib

Files:
 src/cdrom/crc32.cpp
 src/cdrom/dvdisaster.h
 src/cdrom/galois*
 src/cdrom/l-ec.cpp
 src/cdrom/recover-raw.cpp
Comment: dvdisaster
Copyright: 2004-2007 Carsten Gnoerlich
License: GPL-2+

Files: src/hw_sound/ym2612/Ym2612_Emu.cpp
Comment: YM2612
Copyright: 2002 Stephane Dallongeville
           2004-2006 Shay Green
License: LGPL-2.1+

Files: src/desa68/*
Comment: sc68
Copyright: 2001-2003 Benjamin Gerard <ben@sashipa.com>
License: GPL-2+

Files:
 intl/*
 m4/*
 src/gettext.h
Comment: GNU gettext
Copyright: 1984, 1989, 1990, 1995-2007 Free Software Foundation, Inc.
License: LGPL-2+

Files:
 aclocal.m4
 compile
 configure
 depcomp
 INSTALL
 Makefile.in
 missing
Comment: autotools files
Copyright: 1992-2013 Free Software Foundation, Inc.
License: GPL-2+

Files: install-sh
Comment: X11 installation tool
Copyright: 1994 X Consortium
License: X
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to
 deal in the Software without restriction, including without limitation the
 rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 sell copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
 X CONSORTIUM BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
 AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNEC-
 TION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 .
 Except as contained in this notice, the name of the X Consortium shall not
 be used in advertising or otherwise to promote the sale, use or other deal-
 ings in this Software without prior written authorization from the X Consor-
 tium.

Files:
 src/snes/*
Comment: bsnes (see http://byuu.org/articles/emulation-3 for the
 license information)
Copyright: 2004-2011 byuu
           2006-2007 Shay Green
           2008-2010 byuu and nevksti
License: GPL-2

Files:
 src/snes/src/lib/libco/*
Copyright: byuu and the higan team
License: ISC
 Permission to use, copy, modify, and/or distribute this software for
 any purpose with or without fee is hereby granted, provided that the
 above copyright notice and this permission notice appear in all
 copies.
 .
 THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
 WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE
 AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL
 DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR
 PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
 TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 PERFORMANCE OF THIS SOFTWARE.

Files:
 src/cputest/*
Comment: FFmpeg
Copyright: 1997-1999 H. Dietz and R. Fisher
           2000-2002 Fabrice Bellard
           2006 Michael Niedermayer
License: LGPL-2.1+

Files: src/resampler/*
Comment: Speex resampler <http://speex.org/>
Copyright: 2002-2008 Jean-Marc Valin
           2008 Thorvald Natvig
License: BSD-3-clause-Xiph

Files: src/drivers/shader_sabr.inc
Comment: SABR Shader
Copyright: Joshua Street
License: GPL-2+

Files: src/zstd/*
Copyright: 2016-present, Facebook, Inc
License: GPL-2 or BSD-3-clause-Facebook
 This source code is licensed under both the BSD-style license (found in the
 LICENSE file in the root directory of this source tree) and the GPLv2 (found
 in the COPYING file in the root directory of this source tree).
 You may select, at your option, one of the above-listed licenses.
 .
 On Debian systems, the complete text of the GNU General
 Public License version 2 can be found in /usr/share/common-licenses/GPL-2


License: BSD-3-clause-Xiph
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:
 .
 - Redistributions of source code must retain the above copyright
 notice, this list of conditions and the following disclaimer.
 .
 - Redistributions in binary form must reproduce the above copyright
 notice, this list of conditions and the following disclaimer in the
 documentation and/or other materials provided with the distribution.
 .
 - Neither the name of the Xiph.org Foundation nor the names of its
 contributors may be used to endorse or promote products derived from
 this software without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE FOUNDATION
 OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

License: BSD-3-clause-Facebook
 Redistribution and use in source and binary forms, with or without modification,
 are permitted provided that the following conditions are met:
 .
  * Redistributions of source code must retain the above copyright notice, this
    list of conditions and the following disclaimer.
 .
  * Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.
 .
  * Neither the name Facebook nor the names of its contributors may be used to
    endorse or promote products derived from this software without specific
    prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
 ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

License: GPL-2
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License version 2 as
 published by the Free Software Foundation.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License along
 with this program; if not, write to the Free Software Foundation, Inc.,
 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 .
 On Debian systems, the complete text of the GNU General Public License
 version 2 can be found in /usr/share/common-licenses/GPL-2

License: GPL-2+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License along
 with this program; if not, write to the Free Software Foundation, Inc.,
 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 .
 On Debian systems, the complete text of the GNU General Public License
 version 2 can be found in /usr/share/common-licenses/GPL-2

License: LGPL-2+
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Library General Public
 License as published by the Free Software Foundation; either
 version 2 of the License, or (at your option) any later version.
 .
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 .
 You should have received a copy of the GNU Library General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 USA.
 .
 On Debian systems, the complete text of the GNU Library General Public
 License version 2 can be found in /usr/share/common-licenses/LGPL-2

License: LGPL-2.1+
 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 Lesser General Public License for more details.
 .
 You should have received a copy of the GNU Lesser General Public
 License along with this program; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 USA.
 .
 On Debian systems, the complete text of the GNU Lesser General Public
 License version 2.1 can be found in /usr/share/common-licenses/LGPL-2.1

License: Zlib
 This software is provided 'as-is', without any express or implied warranty.
 In no event will the authors be held liable for any damages arising from
 the use of this software.
 .
 Permission is granted to anyone to use this software for any purpose,
 including commercial applications, and to alter it and redistribute it
 freely, subject to the following restrictions:
 .
 1. The origin of this software must not be misrepresented; you must not
    claim that you wrote the original software. If you use this software
    in a product, an acknowledgment in the product documentation would be
    appreciated but is not required.
 .
 2. Altered source versions must be plainly marked as such, and must not
    be misrepresented as being the original software.
 .
 3. This notice may not be removed or altered from any source distribution.
